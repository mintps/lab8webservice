package th.ac.tu.siit.lab8webservice;
import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String,String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	String currentp = "bangkok";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Load Province 
		File infile = getBaseContext().getFileStreamPath("province.tsv");
		if (infile.exists()) { // if file is available
			try { // load data form file and display
				Scanner sc = new Scanner(infile);
				while(sc.hasNextLine()) {
					String line = sc.nextLine();
					currentp = line;
				}
				sc.close();
			} catch (FileNotFoundException e) {
				//Do nothing
			}
		}
		
		setTitle(currentp +" Weather"); //Set the title of the MainActivity
		
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		//Do not allow the layout rotated bases the orientation of the device
		list = new ArrayList<Map<String,String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, 
				new String[] {"name", "value"}, 
				new int[] {R.id.tvName, R.id.tvValue});
		setListAdapter(adapter);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		reload(5,currentp);
		
	}
	
	public void reload(int T,String P)
	{
		currentp = P;
		setTitle(currentp +" Weather"); //Set the title of the MainActivity
		
		//Save Province
		try {
			FileOutputStream outfile = openFileOutput("province.tsv", MODE_PRIVATE); //mode private = only this app can use this file
			PrintWriter p = new PrintWriter(outfile);
			
				p.write(P); // one line of record
			
			p.flush(); p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error: Unable to save data", 
					Toast.LENGTH_SHORT);
			t.show(); 
		}
	
		ConnectivityManager mgr = (ConnectivityManager)
				getSystemService(Context.CONNECTIVITY_SERVICE);
		//Check if the device is connect to a network
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			//The device is connected to the network
			//Load data, we compare the current time and the last update
			//If it is longer than 5 mins since the last update, we load the new data
			long current = System.currentTimeMillis();
			if (current - lastUpdate > T*60*1000) {
				//We start an AsyncTask for loading data
				WeatherTask task = new WeatherTask(this);
				task.execute("http://cholwich.org/"+P+".json");
				//"http://ict.siit.tu.ac.th/~cholwich/bangkok.json"
			}
		}
		else {
			Toast t = Toast.makeText(this, 
					"No Internet Connectivity on this device. Check your setting", 
					Toast.LENGTH_LONG);
			t.show();
		}

	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch(item.getItemId()) {
		case R.id.action_refresh:
			reload(3,currentp);
			return true;
			
		case R.id.action_bangkok:
			reload(0,"bangkok");
			return true;
			
		case R.id.action_nonthaburi:
			reload(0,"nonthaburi");
			return true;
			
		case R.id.action_prathumthani:
			reload(0,"pathumthani");
			return true;
			
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String,String> record;
		ProgressDialog dialog;
		
		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}
		//Executed under the UI thread before the task starts
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		//Executed under the UI thread after the task finished
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss(); //Clear dialog if it displaying
			}
			Toast t = Toast.makeText(getApplicationContext(), 
					result, Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			
		}
		//Execute under the background thread
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				//get the first parameter string, and use it as a URL
				URL url = new URL(params[0]);
				HttpURLConnection http = (HttpURLConnection)url.openConnection();
				//Create a connection to the URL
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				http.setDoInput(true);
				//Read data from the web server
				http.connect();
				
				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(http.getInputStream()));
					while((line = in.readLine()) != null) {
						buffer.append(line);
					}
					//Extract values from the obtains data
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String,String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp));
					list.add(record);
					
					//Extract description
					//"weather": [{ "description": "....."}]
					JSONArray jweather =json.getJSONArray("weather");
					JSONObject w0 =jweather.getJSONObject(0);
					String description = w0.getString("description");
					record = new HashMap<String,String>();
					record.put("name", "Description");
					record.put("value", description);
					list.add(record);
					
					double pressure = jmain.getDouble("pressure");
					//double message = json.getJSONObject("sys").getDouble("message");
					double humidity = jmain.getInt("humidity");
					
					JSONObject jwind =json.getJSONObject("wind");
					double speed = jwind.getDouble("speed");
					double deg = jwind.getDouble("deg");
				
					
					record = new HashMap<String,String>();
					record.put("name", "Pressure");
					record.put("value", String.format(Locale.getDefault(), "%.1f pascal", pressure));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Humidity");
					record.put("value", String.format(Locale.getDefault(), "%.0f %%", humidity));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Temp_Min");
					double mintemp = jmain.getDouble("temp_min")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", mintemp));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Temp_Max");
					double maxtemp = jmain.getDouble("temp_max")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", maxtemp));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Wind Speed");
					record.put("value", String.format(Locale.getDefault(), "%.1f knot", speed));
					list.add(record);
					
					record = new HashMap<String,String>();
					record.put("name", "Wind Deg");
					record.put("value", String.format(Locale.getDefault(), "%.0f degree", deg));
					list.add(record);

					return "Finished Loading Weather Data";
				}
				else {
					return "Error "+response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}	
		}
	}

}

